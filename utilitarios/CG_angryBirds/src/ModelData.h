#ifndef MODEL_DATA_H
#define MODEL_DATA_H

#include <string>
#include <vector>
#include <GL\glew.h>

#define MAX_TEXTURES_NUM 30

class ModelData
{
private:
	GLuint m_vboVertex;
	GLuint m_vboTexture;
	GLuint m_vboNormal;
public:
	GLuint m_vao;
	std::vector<GLuint> textureIDs;
	int numTriangles;

	ModelData();
	~ModelData();

	void loadData(std::string pathObj, std::string pathTexture);
	void loadData(std::string pathObj, std::vector<std::string> pathTextures);
	void removeData();
};

#endif