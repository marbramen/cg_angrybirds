#version 400

in vec3 vertexPosition;

out vec3 textureCoords;

uniform mat4 P, V;

void main()
{
	gl_Position = P * V * vec4(vertexPosition, 1);
	vec3 tmpPosUV = normalize(vertexPosition);
	textureCoords = vec3(tmpPosUV.x, -tmpPosUV.yz);
}
