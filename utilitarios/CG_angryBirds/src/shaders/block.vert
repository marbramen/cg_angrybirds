#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;

// Declare an interface block; see 'Advanced GLSL' for what these are.
out VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} vs_out;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;

void main()
{
    gl_Position = P * V * M * vec4(vertexPosition, 1.0f);
    vs_out.FragPos = vec3(M * vec4(vertexPosition, 1.0));
    vs_out.TexCoords = vertexUV;
    
    mat3 normalMatrix = transpose(inverse(mat3(M)));
    vs_out.Normal = normalMatrix * vertexNormal;
}