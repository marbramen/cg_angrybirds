#include "Character.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>

using namespace glm;

Character::Character()
{
	center = glm::vec3(0,0,0);
	radio = 0.0;
	velocity = glm::vec3(0, 0, 0);
	dt = 0.5;

	isInited = false;
	m_vao = 0;
	m_textureIDs.clear();
	gPosition = glm::vec3(0, 0, 0);
	gOrientation = glm::vec3(0, 0, 0);
	gScale = glm::vec3(1.0f, 1.0f, 1.0f);
}

void Character::init(ModelData *data)
{
	if (data == NULL) {
		isInited = false;
		return;
	}
	m_vao = data->m_vao;
	m_textureIDs = data->textureIDs;
	numTriangles = data->numTriangles;
	isInited = true;
}

void Character::draw(ShaderInfo shader)
{
	if (!isInited) {
		printf("Please load the resource of the character before draw()\n");
		return;
	}
	
	// Usar el shader del objeto
	glUseProgram(shader.shaderID);
	
	// Modifying matrices
	glm::mat4 RotationMatrix = eulerAngleYXZ(gOrientation.y, gOrientation.x, gOrientation.z);
	glm::mat4 TranslationMatrix = translate(mat4(), gPosition);
	glm::mat4 ScalingMatrix = scale(mat4(), gScale);
	glm::mat4 ModelMatrix = TranslationMatrix * RotationMatrix * ScalingMatrix;
	
	glUniformMatrix4fv(shader.uniformIDs[0], 1, GL_FALSE, &ModelMatrix[0][0]);
	glUniformMatrix4fv(shader.uniformIDs[1], 1, GL_FALSE, &View[0][0]);
	glUniformMatrix4fv(shader.uniformIDs[2], 1, GL_FALSE, &Projection[0][0]);

	if (shader.uniformIDs.size() > 3) {
		glUniform3fv(shader.uniformIDs[3], 1, &lightPos[0]);
		glUniform3fv(shader.uniformIDs[4], 1, &cameraPosition[0]);
		glUniform1i(shader.uniformIDs[5], normalMapping);
	}
	
	// Using more than one texture for the object
	for (auto i = 0; i < m_textureIDs.size(); i++) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, m_textureIDs[i]);
		glUniform1i(shader.textureIDs[i], i);
	}

	// Drawing the character, VAO stores VBOs, only need this value to draw the characters
	glBindVertexArray(m_vao);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	
	glDrawArrays(GL_TRIANGLES, 0, numTriangles);
	
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Character::cleanup()
{
	if (!isInited) {
		return;
	}
	isInited = false;
	m_vao = 0;
	numTriangles = 0;
	m_textureIDs.clear();
}

void Character::move(glm::vec3 despl) 
{
	// e = v*t
	glm::vec3 e = glm::vec3(despl.x * dt, despl.y * dt, despl.z * dt);
	gPosition += e;
	center += e;
}

void Character::setNormalMapping(bool state)
{
	normalMapping = state;
}

void Character::setPosition(glm::vec3 newPos)
{
	gPosition = newPos;
}

void Character::setOrientation(glm::vec3 newOr)
{
	gOrientation = vec3(radians(newOr.x), radians(newOr.y), radians(newOr.z));
}

void Character::setScale(glm::vec3 newScale)
{
	gScale = newScale;
}

void Character::setCenter(glm::vec3 newCenter) {
	center = newCenter;
}

void Character::setRadio(double newRadio){
	radio = newRadio;
}

glm::vec3 Character::getCenter(){
	return center;
}

double Character::getRadio(){
	return radio;
}

void Character::setVelocity(glm::vec3 newVelocity){
	velocity = newVelocity;
}

glm::vec3 Character::getVelocity(){
	return velocity;
}