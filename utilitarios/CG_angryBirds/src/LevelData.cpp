#include "LevelData.h"
#include "Loader3D.h"

using namespace std;

LevelData::LevelData()
{
	skybox_vao = 0;
	skyboxTextureID = 0;
	skybox_vboVertex = 0;
}

LevelData::~LevelData()
{
}

void LevelData::loadBackground(const unsigned int sizeEnvironment, std::vector<string> environment)
{
	skyboxTextureID = loadSkyboxTexture(environment);

	GLfloat size = sizeEnvironment * 0.5f;
	GLfloat skyboxVertices[] = {
		-size,  size, -size,
		-size, -size, -size,
		size, -size, -size,
		size, -size, -size,
		size,  size, -size,
		-size,  size, -size,

		-size, -size,  size,
		-size, -size, -size,
		-size,  size, -size,
		-size,  size, -size,
		-size,  size,  size,
		-size, -size,  size,

		size, -size, -size,
		size, -size,  size,
		size,  size,  size,
		size,  size,  size,
		size,  size, -size,
		size, -size, -size,

		-size, -size,  size,
		-size,  size,  size,
		size,  size,  size,
		size,  size,  size,
		size, -size,  size,
		-size, -size,  size,

		-size,  size, -size,
		size,  size, -size,
		size,  size,  size,
		size,  size,  size,
		-size,  size,  size,
		-size,  size, -size,

		-size, -size, -size,
		-size, -size,  size,
		size, -size, -size,
		size, -size, -size,
		-size, -size,  size,
		size, -size,  size
	};

	glGenVertexArrays(1, &skybox_vao);
	glBindVertexArray(skybox_vao);
	
	glGenBuffers(1, &skybox_vboVertex);
	glBindBuffer(GL_ARRAY_BUFFER, skybox_vboVertex);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	
	glBindVertexArray(0);
}

void LevelData::loadDataElem(std::string type, TiXmlElement* levelElems) {
	TiXmlElement *itemData = levelElems->FirstChildElement();
	vector<LevelComponent> elements;
	while (itemData != NULL)
	{
		LevelComponent item;
		item.type = itemData->Attribute("type");
		itemData->QueryFloatAttribute("x", &item.pos.x);
		itemData->QueryFloatAttribute("y", &item.pos.y);
		itemData->QueryFloatAttribute("z", &item.pos.z);
		itemData->QueryFloatAttribute("rx", &item.orientation.x);
		itemData->QueryFloatAttribute("ry", &item.orientation.y);
		itemData->QueryFloatAttribute("rz", &item.orientation.z);
		itemData->QueryFloatAttribute("sx", &item.scale.x);
		itemData->QueryFloatAttribute("sy", &item.scale.y);
		itemData->QueryFloatAttribute("sz", &item.scale.z);
		elements.push_back(item);
		itemData = itemData->NextSiblingElement();
	}
	components[type] = elements;
}

void LevelData::loadData(TiXmlHandle *levelData)
{
	unsigned int sizeWorld;
	vector<string> textures;
	TiXmlElement *levelAttrs = levelData->Element();
	levelAttrs->QueryUnsignedAttribute("size", &sizeWorld);
	string pathBackground = levelAttrs->Attribute("background");
	textures.push_back(pathBackground + levelAttrs->Attribute("right"));
	textures.push_back(pathBackground + levelAttrs->Attribute("left"));
	textures.push_back(pathBackground + levelAttrs->Attribute("bottom"));
	textures.push_back(pathBackground + levelAttrs->Attribute("top"));
	textures.push_back(pathBackground + levelAttrs->Attribute("front"));
	textures.push_back(pathBackground + levelAttrs->Attribute("back"));

	loadBackground(sizeWorld, textures);
	loadDataElem("birds", levelData->ChildElement("birds", 0).Element());
	loadDataElem("pigs", levelData->ChildElement("pigs", 0).Element());
	loadDataElem("blocks", levelData->ChildElement("blocks", 0).Element());
	//loadDataElem("spheres", levelData->ChildElement("spheres", 0).Element());
}

void LevelData::removeData()
{
	if (skybox_vboVertex) {
		glDeleteBuffers(1, &skybox_vboVertex);
	}
	if (skybox_vao) {
		glDeleteVertexArrays(1, &skybox_vao);
	}
}
