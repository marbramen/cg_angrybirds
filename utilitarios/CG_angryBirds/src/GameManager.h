#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <string>
#include "GlobalVariables.h"
#include "Character.h"
#include "ResourceManager.h"
#include "LevelManager.h"

class GameManager
{
private:
	static GameManager *sInstance;
	LEVEL_ID currentLevel;
	TiXmlDocument* doc;
	
	ShaderInfo skyboxShader;
	ShaderInfo modelShader;
	ShaderInfo blockShader;
	
	void loadShaders();
	void loadResources(std::string configFile);
	void startGame();
	void removeShaders();

public:
	~GameManager();

	static GameManager *getInstance();

	void initGame(std::string configFile);
	void nextLevel();
	void renderGame();
	void stopGame();
};

#endif