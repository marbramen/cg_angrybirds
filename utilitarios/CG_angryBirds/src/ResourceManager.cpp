#include "ResourceManager.h"
#include <vector>
#include <iostream>

using namespace std;

ResourceManager * ResourceManager::sInstance = NULL;

ResourceManager* ResourceManager::getInstance()
{
	if (sInstance == NULL) {
		sInstance = new ResourceManager();
	}
	return sInstance;
}

ResourceManager::~ResourceManager()
{
	removeModels();
}

bool ResourceManager::loadModel(std::string idModel, std::string pathObj, std::string pathTexture)
{
	if (models.find(idModel) != models.end())
		return true;

	ModelData modelLoaded;
	modelLoaded.loadData(pathObj, pathTexture);
	models[idModel] = modelLoaded;

	return true;
}

ModelData *ResourceManager::getModel(std::string idModel)
{
	std::map<std::string, ModelData>::iterator it = models.find(idModel);
	if (it != models.end())
		return &(it->second);
	return NULL;
}

void ResourceManager::removeModel(std::string idModel)
{
	std::map<std::string, ModelData>::iterator it = models.find(idModel);
	if (it != models.end()) {
		it->second.removeData();
		models.erase(it);
	}
}

void ResourceManager::loadModels(TiXmlElement* modelsGroup)
{
	TiXmlElement* modelElem = modelsGroup->FirstChildElement();
	std::string idModel, pathObj, pathTexture;
	std::cout << "entro ps a loadModels " << std::endl;
	while (modelElem != NULL)
	{
		std::cout << "entro ps a loadModels ==> " << std::endl;

		idModel = modelElem->Attribute("type");
		pathObj = modelElem->Attribute("mesh");
		pathTexture = modelElem->Attribute("texture");

		loadModel(idModel, pathObj, pathTexture);
		modelElem = modelElem->NextSiblingElement();
	}
}

void ResourceManager::loadBlocks(TiXmlElement* blocksGroup)
{
	std::string cubeObjPath;
	TiXmlElement* modelElem = blocksGroup->FirstChildElement();
	std::string idModel, pathTexture, pathNormalMap;
	std::vector<std::string> texturePaths;

	while (modelElem != NULL)
	{
		idModel = modelElem->Attribute("type");
		cubeObjPath = modelElem->Attribute("mesh");
		texturePaths.clear();
		texturePaths.push_back(modelElem->Attribute("texture"));
		texturePaths.push_back(modelElem->Attribute("normal_map"));

		if (models.find(idModel) == models.end())
		{
			ModelData modelLoaded;
			modelLoaded.loadData(cubeObjPath, texturePaths);
			models[idModel] = modelLoaded;
		}
		modelElem = modelElem->NextSiblingElement();
	}
}

void ResourceManager::removeModels()
{
	for (std::map<std::string, ModelData>::iterator it = models.begin(); it != models.end(); it++)
	{
		it->second.removeData();
	}
	models.clear();
}

unsigned int ResourceManager::numModelsLoaded()
{
	return models.size();
}