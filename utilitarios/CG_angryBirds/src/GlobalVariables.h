#ifndef GLOBAL_VARIABLES_H
#define GLOBAL_VARIABLES_H

#include <glm\glm.hpp>
#include <GL\glew.h>
#include <vector>

extern glm::mat4 Projection;
extern glm::mat4 View;
extern glm::vec3 lightPos;
extern glm::vec3 cameraPosition;

struct ShaderInfo {
	GLuint shaderID;
	std::vector<GLuint> uniformIDs;
	std::vector<GLuint> textureIDs;
};

inline void removeShader(ShaderInfo shader)
{
	glDeleteProgram(shader.shaderID);
	for (unsigned int i = 0; i < shader.textureIDs.size(); i++) {
		glDeleteTextures(1, &shader.textureIDs[i]);
	}
}

#endif