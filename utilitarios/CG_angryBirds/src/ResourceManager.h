#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <map>
#include <TinyXML\tinyxml.h>
#include "ModelData.h"

class ResourceManager
{
public:
	~ResourceManager();
	static ResourceManager* getInstance();
	
	bool loadModel(std::string idModel, std::string pathObj, std::string pathTexture);
	ModelData *getModel(std::string idModel);
	void removeModel(std::string idModel);
	void loadModels(TiXmlElement *modelsGroup);
	void loadBlocks(TiXmlElement *blocksGroup);
	void removeModels();
	unsigned int numModelsLoaded();

private:
	static ResourceManager *sInstance;
	std::map<std::string, ModelData> models;
};

#endif
