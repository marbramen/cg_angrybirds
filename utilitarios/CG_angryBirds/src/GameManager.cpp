#include "GameManager.h"
#include "LevelManager.h"
#include "shader.h"


GameManager *GameManager::sInstance = NULL;

GameManager::~GameManager()
{
}

GameManager *GameManager::getInstance()
{
	if (sInstance == NULL) {
		sInstance = new GameManager();
	}
	return sInstance;
}

void GameManager::loadShaders()
{
	// Cargando shaders para los objetos
	modelShader.shaderID = LoadShaders("shaders/modelVertShader.vert", "shaders/modelFragShader.frag");
	modelShader.uniformIDs.push_back(glGetUniformLocation(modelShader.shaderID, "M"));
	modelShader.uniformIDs.push_back(glGetUniformLocation(modelShader.shaderID, "V"));
	modelShader.uniformIDs.push_back(glGetUniformLocation(modelShader.shaderID, "P"));
	modelShader.textureIDs.push_back(glGetUniformLocation(modelShader.shaderID, "myTexture"));

	// Cargando shaders para los bloques
	blockShader.shaderID = LoadShaders("shaders/block.vert", "shaders/block.frag");
	blockShader.uniformIDs.push_back(glGetUniformLocation(blockShader.shaderID, "M"));
	blockShader.uniformIDs.push_back(glGetUniformLocation(blockShader.shaderID, "V"));
	blockShader.uniformIDs.push_back(glGetUniformLocation(blockShader.shaderID, "P"));
	blockShader.uniformIDs.push_back(glGetUniformLocation(blockShader.shaderID, "lightPos"));
	blockShader.uniformIDs.push_back(glGetUniformLocation(blockShader.shaderID, "viewPos"));
	blockShader.uniformIDs.push_back(glGetUniformLocation(blockShader.shaderID, "normalMapping"));
	blockShader.textureIDs.push_back(glGetUniformLocation(blockShader.shaderID, "diffuseMap"));
	blockShader.textureIDs.push_back(glGetUniformLocation(blockShader.shaderID, "normalMap"));

	// Cargando shaders para el background
	skyboxShader.shaderID = LoadShaders("shaders/backgroundVertShader.vert", "shaders/backgroundFragShader.frag");
	skyboxShader.uniformIDs.push_back(glGetUniformLocation(skyboxShader.shaderID, "V"));
	skyboxShader.uniformIDs.push_back(glGetUniformLocation(skyboxShader.shaderID, "P"));
	skyboxShader.textureIDs.push_back(glGetUniformLocation(skyboxShader.shaderID, "skybox"));
}

void GameManager::loadResources(std::string configFile)
{
	doc = new TiXmlDocument(configFile.c_str());
	
	if (doc->LoadFile()) {
		TiXmlHandle docHandle(doc);
		ResourceManager::getInstance()->loadModels(docHandle.FirstChildElement().ChildElement("birds", 0).Element());
		ResourceManager::getInstance()->loadModels(docHandle.FirstChildElement().ChildElement("pigs", 0).Element());
		ResourceManager::getInstance()->loadBlocks(docHandle.FirstChildElement().ChildElement("blocks", 0).Element());
		ResourceManager::getInstance()->loadModels(docHandle.FirstChildElement().ChildElement("spheres", 0).Element());
		LevelManager::getInstance()->loadLevels(&docHandle.FirstChildElement().ChildElement("levels", 0));
		printf(">> Models loaded = %d\n", ResourceManager::getInstance()->numModelsLoaded());
		printf(">> Levels loaded = %d\n", LevelManager::getInstance()->numLevelsLoaded());
	}
}

void GameManager::startGame() 
{

	// Enviar los objects y lo que se requiera


	// Activar movimiento 


	// Deteccion de  colisiones


	// Solucion de colisiones


}

void GameManager::initGame(std::string configFile)
{
	loadShaders();
	loadResources(configFile);
	currentLevel = 1;
	LevelManager::getInstance()->setCurrentLevel(currentLevel);
}

void GameManager::nextLevel()
{
	currentLevel++;
	if (currentLevel > LevelManager::getInstance()->numLevelsLoaded())
		currentLevel--;
	printf("Current Level: %d\n", currentLevel);
	LevelManager::getInstance()->setCurrentLevel(currentLevel);
}

void GameManager::renderGame()
{
	LevelManager::getInstance()->drawBackground(skyboxShader);
	//LevelManager::getInstance()->drawCharacters(modelShader);
	//LevelManager::getInstance()->drawCharacters1(modelShader);
	//LevelManager::getInstance()->drawCharacters2(modelShader);

	LevelManager::getInstance()->drawCharacters2(blockShader);
	LevelManager::getInstance()->drawBlocks(blockShader);
	LevelManager::getInstance()->drawSpheres(blockShader);
	//LevelManager::getInstance()->drawBlocks(modelShader);

}

void GameManager::stopGame()
{
	ResourceManager::getInstance()->removeModels();
	LevelManager::getInstance()->removeLevels();
	removeShaders();
}

void GameManager::removeShaders()
{
	removeShader(modelShader);
	removeShader(blockShader);
	removeShader(skyboxShader);
}