#include <vector>

using namespace std;


/*
# is a comment, just like // in C++
usemtl and mtllib describe the look of the model. We won’t use this in this tutorial.
v is a vertex
vt is the texture coordinate of one vertex
vn is the normal of one vertex
f is a face ((starts in 1 vertex id)/(texture coordinate)/(normal)) 
*/

bool loadOBJ(
    const char * path,
    std::vector < glm::vec3 > & out_vertices,
    std::vector < glm::vec2 > & out_uvs,
    std::vector < glm::vec3 > & out_normals
	
	std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
	std::vector< glm::vec3 > temp_vertices;
	std::vector< glm::vec2 > temp_uvs;
	std::vector< glm::vec3 > temp_normals;
	
	
)

