#include <string>
#include <map>
#include <vector>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <TinyXML\tinyxml.h>

struct LevelComponent {
	std::string type;
	glm::vec3 pos, 
			scale, 
			orientation;
};

class LevelData
{
private:
	GLuint skybox_vboVertex;
	
	void loadBackground(const unsigned int sizeEnvironment, std::vector<std::string> environment);
	void loadDataElem(std::string type, TiXmlElement* levelData);
public:
	GLuint skybox_vao;
	GLuint skyboxTextureID;
	std::map<std::string, std::vector<LevelComponent> > components;

	LevelData();
	~LevelData();

	void loadData(TiXmlHandle *levelData);
	void removeData();
};

