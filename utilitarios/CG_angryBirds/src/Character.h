#ifndef CHARACTER_H
#define CHARACTER_H

#include <glm\glm.hpp>
#include <vector>
#include "ModelData.h"
#include "GlobalVariables.h"

class Character {
private:
	bool isInited;
	bool normalMapping;

	// for collisions
	glm::vec3 center;
	glm::vec3 velocity;
	double radio;
	double dt;

	GLuint m_vao;
	std::vector<GLuint> m_textureIDs;

	int numTriangles;
	glm::vec3 gPosition,
		gOrientation,
		gScale;
public:
	Character();
	void init(ModelData *data);
	void draw(ShaderInfo shader);
	void cleanup();

	void move(glm::vec3 despl);
	void setNormalMapping(bool state);
	void setPosition(glm::vec3 newPos);
	void setOrientation(glm::vec3 newOr);
	void setScale(glm::vec3 newScale);
	void setVelocity(glm::vec3 newVelocity);

	void setCenter(glm::vec3 newCenter);
	void setRadio(double newRadio);

	glm::vec3 getCenter();
	double    getRadio();
	glm::vec3 getVelocity();

};

#endif