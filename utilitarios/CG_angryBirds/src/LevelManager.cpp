#include "LevelManager.h"
#include "ResourceManager.h"
#include "GlobalVariables.h"
#include <iostream>
#include <math.h> 
#define dbg(x) cout << #x <<"="<< x << endl
#define dbg2(x,y) cout << #x <<"="<< x << " " << #y << "=" << y <<endl
#define dbg3(x,y,z) cout << #x <<"="<< x << " " << #y << "=" << y << " " << #z << "="<< z << endl

using namespace std;

#define MY_ELIPSON 1.0e-8

inline bool myAbs(double x){
	return x < 0 ? -1 * x : x;
}

LevelManager* LevelManager::sInstance = NULL;

LevelManager* LevelManager::getInstance()
{
	if (sInstance == NULL) {
		sInstance = new LevelManager();
	}
	return sInstance;
}

LevelManager::~LevelManager()
{
	levels.clear();
}

void LevelManager::loadLevels(TiXmlHandle *levelsGroup)
{
	TiXmlElement* levelElem = levelsGroup->Element()->FirstChildElement();
	TiXmlHandle levelNode = NULL;
	LEVEL_ID idLevel;

	while (levelElem != NULL)
	{
		levelElem->QueryUnsignedAttribute("number", &idLevel);
		if (levels.find(idLevel) == levels.end())
		{
			LevelData levelLoaded;
			levelLoaded.loadData(&levelsGroup->ChildElement("level", idLevel - 1));
			levels[idLevel] = levelLoaded;
		}
		levelElem = levelElem->NextSiblingElement();
	}
}

void LevelManager::removeLevels()
{
	for (std::map<LEVEL_ID, LevelData>::iterator it = levels.begin(); it != levels.end(); it++)
	{
		it->second.removeData();
	}
	levels.clear();
}

void LevelManager::createCurrentLevel()
{
	birds.clear();
	pigs.clear();
	blocks.clear();
	spheres.clear();

	vector<LevelComponent> birdsData = levels[currentLevel].components["birds"];
	vector<LevelComponent> pigsData = levels[currentLevel].components["pigs"];
	vector<LevelComponent> blocksData = levels[currentLevel].components["blocks"];
	vector<LevelComponent> spheresData = levels[currentLevel].components["spheres"];
	
	cout << " len of Spheresdata " << spheresData.size() << endl;
	
	for (size_t i = 0; i < birdsData.size(); i++) {
		Character bird;
		bird.init(ResourceManager::getInstance()->getModel(birdsData[i].type));
		bird.setPosition(birdsData[i].pos);
		glm::vec3 scaleVec3(0.5f, 0.5f, 0.5f);
		bird.setScale(scaleVec3);
		bird.setOrientation(birdsData[i].orientation);
		bird.setCenter(birdsData[i].pos);
		bird.setRadio(10.0);
		//bird.setVelocity(glm::vec3(0, 0, -2));
		bird.setVelocity(glm::vec3(-5, -5, 0));
		birds.push_back(bird);
	}
	for (size_t i = 0; i < pigsData.size(); i++) {
		Character pig;
		pig.init(ResourceManager::getInstance()->getModel(pigsData[i].type));
		pig.setPosition(pigsData[i].pos);		
		glm::vec3 scaleVec3(0.5f, 0.5f, 0.5f);
		//pig.setScale(pigsData[i].scale);
		pig.setScale(scaleVec3);
		pig.setOrientation(pigsData[i].orientation);
		pig.setCenter(pigsData[i].pos);
		pig.setRadio(10.0);
		////pig.setVelocity(glm::vec3(0,0,2));
		pig.setVelocity(glm::vec3(-5, 0, 0));
		//pig.setVelocity(glm::vec3(0,0,1));
		pigs.push_back(pig);
	}
	for (size_t i = 0; i < blocksData.size(); i++) {
		Character block;
		block.init(ResourceManager::getInstance()->getModel(blocksData[i].type));
		glm::vec3 scaleVec3(0.5f, 0.5f, 0.5f);
		//block.setScale(pigsData[i].scale);
		block.setScale(scaleVec3);
		block.setNormalMapping(true);
		block.setPosition(blocksData[i].pos);
		//block.setScale(blocksData[i].scale);
		block.setOrientation(blocksData[i].orientation);
		block.setCenter(blocksData[i].pos);
		block.setRadio(10.0);	
		blocks.push_back(block);
	}
	for (size_t i = 0; i < spheresData.size(); i++) {
		Character sphere;
		sphere.init(ResourceManager::getInstance()->getModel(spheresData[i].type));
		sphere.setPosition(spheresData[i].pos);
		//sphere.setScale(spheresData[i].scale);
		sphere.setOrientation(spheresData[i].orientation);
		sphere.setCenter(spheresData[i].pos);
		sphere.setRadio(10.0);		
		spheres.push_back(sphere);
	}
}

void LevelManager::setCurrentLevel(LEVEL_ID idLevel)
{
	currentLevel = idLevel;
	createCurrentLevel();
}

void LevelManager::drawBackground(ShaderInfo shader)
{
	glDepthMask(GL_FALSE);
	glUseProgram(shader.shaderID);
	glUniform1i(shader.textureIDs[0], 0);

	// Loading View and Projection matrices
	glUniformMatrix4fv(shader.uniformIDs[0], 1, GL_FALSE, &View[0][0]);
	glUniformMatrix4fv(shader.uniformIDs[1], 1, GL_FALSE, &Projection[0][0]);
	
	// Draw skybox
	glBindVertexArray(levels[currentLevel].skybox_vao);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, levels[currentLevel].skyboxTextureID);
	
	glEnableVertexAttribArray(0);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
	glDepthMask(GL_TRUE);
}

bool LevelManager::isCollision(glm::vec3 C1, double r1, glm::vec3 C2, double r2){
	double dx = C1.x - C2.x;
	double dy = C1.y - C2.y;
	double dz = C1.z - C2.z;

	double distance = std::sqrt(dx * dx + dy * dy + dz*dz);

	if (distance < r1 + r2)
		return 1;

	return 0;
}

double LevelManager::modulo(glm::vec3 v){
	return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

double LevelManager::crossProduct(glm::vec3 a, glm::vec3 b){
	return a.x*b.x + a.y*b.y + a.z*b.z;
}


void LevelManager::drawCharacters(ShaderInfo shader)
{
	// Move Models
	for (size_t i = 0; i < pigs.size(); i++) {
		pigs[i].move(pigs[i].getVelocity());
		cout << "velocidad del cerdo " << i << "x:" << pigs[i].getVelocity().x << " y:" << pigs[i].getVelocity().y << " z:" << pigs[i].getVelocity().z << endl;
	}
	for (size_t i = 0; i < birds.size(); i++) {
		birds[i].move(birds[i].getVelocity());
		cout << "velocidad del cerdo " << i << "x:" << birds[i].getVelocity().x << " y:" << birds[i].getVelocity().y << " z:" << birds[i].getVelocity().z << endl;
	}

	// Detec collision
	for (size_t i = 0; i < birds.size(); i++) {
		for (size_t j = 0; j < pigs.size(); j++) {
			if (isCollision(pigs[j].getCenter(), pigs[j].getRadio(), birds[i].getCenter(), birds[i].getRadio())) {
				
				glm::vec3 C1 = pigs[j].getCenter();
				glm::vec3 V1 = pigs[j].getVelocity();

				glm::vec3 C2 = birds[i].getCenter();
				glm::vec3 V2 = birds[i].getVelocity();

				// Reaccion del choque
				pigs[j].setVelocity(glm::vec3(0, 1, 0));
				birds[i].setVelocity(glm::vec3(1, 0, 0));
				// hallamos las nuevas fuerzas

				//dbg3(C2.x, C2.y, C2.z);
				//dbg3(C1.x, C1.y, C1.z);
				//dbg3(V1.x,V1.y,V1.z);
				//dbg3(V2.x, V2.y, V2.z);

				//double temp1 = (C1.x*V1.x + C1.y*V1.y + C1.z*V1.z) / (sqrt((C1.x + C1.y + C1.z) * (V1.x + V1.y + V1.z)));
				//double temp2 = (C2.x*V2.x + C2.y*V2.y + C2.z*V2.z) / (sqrt((C2.x + C2.y + C2.z) * (V2.x + V2.y + V2.z)));

				//double angulo1 = acos(temp1);
				//double angulo2 = acos(temp2);
				//
				//glm::vec3 N1(V1.x*cos(angulo1), V1.y*cos(angulo1), V1.z*cos(angulo1)); //V1 * cos(angulo1);
				//glm::vec3 N2(V1.x*cos(angulo1), V1.y*cos(angulo1), V1.z*cos(angulo1)); //V2 * cos(angulo2);

				//// Hallando modulos
				//double n1 = modulo(N1);
				//double n2 = modulo(N2);
				//dbg(n1);
				//dbg(n2);

				//Descomposicion de los vectores en x,y,z
				// x = r*sen()*cos()
				// y = r*sen()*sen()
				// z = r*cos()

				// Hallamos angulo entre z positivo y vector
				// angulo eje positivo x y la proyeccion del vector en xy

				/*glm::vec3 Center1;
				Center1.x = n1*sin(2)*cos(2);
				Center1.y = n1*sin(3)*sin(3);
				Center1.z = n1*cos(3);

				glm::vec3 Center2;
				Center2.x = n1*sin(3)*cos(3);
				Center2.y = n1*sin(3)*sin(3);
				Center2.z = n1*cos(3);

				glm::vec3 Cnormal1 = V1 - Center1;
				glm::vec3 Cnormal2 = V2 - Center1;

				pigs[j].setVelocity(Cnormal1);
				birds[i].setVelocity(Cnormal2);*/

			}
		}
	}

	// Draw Models
	for (size_t i = 0; i < pigs.size(); i++) {
		pigs[i].draw(shader);
	}
	for (size_t i = 0; i < birds.size(); i++) {
		birds[i].draw(shader);
	}
}

void imprimirVec3(string nombre, glm::vec3 vector){
	cout << nombre << "x: " << vector.x << " y: " << vector.y << " z: " << vector.z << endl;
}

void LevelManager::drawCharacters1(ShaderInfo shader){
	// este codigo solo trabaja para 2d
	for (size_t i = 0; i < pigs.size(); i++) {
		pigs[i].move(pigs[i].getVelocity());
		//cout << "velocidad del cerdo " << i << "x:" << pigs[i].getVelocity().x << " y:" << pigs[i].getVelocity().y << " z:" << pigs[i].getVelocity().z << endl;
	}
	for (size_t i = 0; i < birds.size(); i++) {
		birds[i].move(birds[i].getVelocity());
		//cout << "velocidad del ave " << i << "x:" << birds[i].getVelocity().x << " y:" << birds[i].getVelocity().y << " z:" << birds[i].getVelocity().z << endl;
	}

	for (size_t i = 0; i < birds.size(); i++) {
		for (size_t j = 0; j < pigs.size(); j++) {
			if (isCollision(pigs[j].getCenter(), pigs[j].getRadio(), birds[i].getCenter(), birds[i].getRadio())) {
				glm::vec3 distBetCens = glm::vec3(pigs[j].getCenter().x - birds[i].getCenter().x, pigs[j].getCenter().y - birds[i].getCenter().y, pigs[j].getCenter().z - birds[i].getCenter().z);
												
				// obtenemos el vector perpendicular
				glm::vec3 perpenLine = glm::vec3(-distBetCens.y, distBetCens.x, 0);
				double param = crossProduct(distBetCens, perpenLine) / (modulo(perpenLine) * modulo(pigs[j].getVelocity()));
				double angle = acos(param) * 180.0 / 3.14159265;

				double coef = crossProduct(perpenLine, pigs[j].getVelocity()) / modulo(perpenLine);
				glm::vec3 velocity1 = glm::vec3(coef * pigs[j].getVelocity().x, coef*pigs[j].getVelocity().y, coef*pigs[j].getVelocity().z);
				glm::vec3 velocity2 = glm::vec3(pigs[j].getVelocity().x - velocity1.x, pigs[j].getVelocity().y - velocity1.y, pigs[j].getVelocity().z - velocity1.z);

				
				imprimirVec3("distBetCens", distBetCens);
				imprimirVec3("cerdo ", pigs[j].getCenter());
				imprimirVec3("bird ", birds[j].getCenter());
				imprimirVec3("perpenLine ", perpenLine);
				imprimirVec3("velocidad Cerdo antes ", pigs[j].getVelocity());
				imprimirVec3("velocidad Ave antes ", birds[i].getVelocity());
				imprimirVec3("velocidad Cerdo despues ", velocity1);
				imprimirVec3("velocidad Ave despues ", velocity2);

				pigs[j].setVelocity(velocity1);
				birds[i].setVelocity(velocity2);


			}
		}
	}
	
	// Draw Models
	for (size_t i = 0; i < pigs.size(); i++) {
		pigs[i].draw(shader);
	}
	for (size_t i = 0; i < birds.size(); i++) {
		birds[i].draw(shader);
	}

}

glm::vec3 prodScalar(glm::vec3 x, double k){
	return glm::vec3(k*x.x, k*x.y, k*x.z);
}

glm::vec3 operator *(double k, glm::vec3 A){
	return glm::vec3(k*A.x, k*A.y, k*A.z);
}

glm::vec3 operator +(const glm::vec3& A, const glm::vec3& B) {
	return glm::vec3(A.x + B.x, A.y + B.y, A.z + B.z);
}

glm::vec3 operator -(const glm::vec3& A, const glm::vec3& B) {
	return glm::vec3(A.x - B.x, A.y - B.y, A.z - B.z);
}

void LevelManager::drawCharacters2(ShaderInfo shader){
	for (size_t i = 0; i < pigs.size(); i++) {
		pigs[i].move(pigs[i].getVelocity());		
	}
	for (size_t i = 0; i < birds.size(); i++) {
		birds[i].move(birds[i].getVelocity());
		imprimirVec3("centro del birds ", birds[i].getCenter());
	}

	for (size_t i = 0; i < birds.size(); i++) {
		for (size_t j = 0; j < pigs.size(); j++) {
			if (isCollision(pigs[j].getCenter(), pigs[j].getRadio(), birds[i].getCenter(), birds[i].getRadio())) {				
								
				/*  MODELO 1 Movs vs Est*/
				//glm::vec3 distBetCens = glm::vec3(birds[i].getCenter().x - pigs[j].getCenter().x, birds[i].getCenter().y - pigs[j].getCenter().y, birds[i].getCenter().z - pigs[j].getCenter().z);
				//double k = modulo(pigs[j].getVelocity()) / modulo(distBetCens);
				//glm::vec3 forBirds = k * distBetCens;
				//glm::vec3 normalN = glm::vec3(-1 * distBetCens.x, -1 * distBetCens.y, -1 * distBetCens.z);

				//glm::vec3 uniNormalN = (1 / modulo(normalN)) * normalN;
				//glm::vec3 uniIN = (1 / modulo(pigs[j].getVelocity())) * pigs[j].getVelocity();
				//glm::vec3 reflex = ((2 * crossProduct(-1.0 * uniIN, uniNormalN)) * uniNormalN) + uniIN;
				// 
				//reflex = modulo(pigs[j].getVelocity()) * reflex;
	
				//birds[i].setVelocity(forBirds);				
				//pigs[j].setVelocity(reflex);

				/* MODELO 2 Movs vs Movs*/
				glm::vec3 Fu = birds[i].getVelocity();
				glm::vec3 Fv = pigs[j].getVelocity();
				glm::vec3 c1 = birds[i].getCenter();
				glm::vec3 c2 = pigs[j].getVelocity();

				glm::vec3 xAxis = (1 / modulo(c1 - c2))*(c1 - c2);
				glm::vec3 uXp = crossProduct(Fu, xAxis) * xAxis;
				glm::vec3 uYp = Fu - uXp;
				glm::vec3 vXp = crossProduct(Fv, -1 * xAxis) * (-1 * xAxis);
				glm::vec3 vYp = Fv - vXp;

				glm::vec3 uXp_new = vXp;
				glm::vec3 uYp_new = uYp;
				glm::vec3 vXp_new = uXp;
				glm::vec3 vYp_new = vYp;

				Fu = uXp_new + uYp_new;
				Fv = vXp_new + vYp_new;

				birds[i].setVelocity(Fu);
				pigs[j].setVelocity(Fv);				
			}
		}
	}


	for (size_t i = 0; i < pigs.size(); i++) {
		pigs[i].draw(shader);
	}
	for (size_t i = 0; i < birds.size(); i++) {
		birds[i].draw(shader);
	}
}

bool isCero(glm::vec3 v1){
	return myAbs(v1.x) <= MY_ELIPSON && myAbs(v1.y) <= MY_ELIPSON && myAbs(v1.z) <= MY_ELIPSON;
}

bool equalVect(glm::vec3 &v1, glm::vec3 &v2){
	double dx = v1.x - v2.x;
	double dy = v1.y - v2.y;
	double dz = v1.z - v2.z;
	return isCero(glm::vec3(myAbs(v1.x - v2.x), myAbs(v1.y - v2.y), myAbs(v1.z - v2.z)));
}

void LevelManager::drawCharacters3(ShaderInfo shader){
	std::vector<Character> objs;
	for (size_t i = 0; i < pigs.size(); i++)
		objs.push_back(pigs[i]);
	for (size_t i = 0; i < birds.size(); i++)
		objs.push_back(birds[i]);

	for (size_t i = 0; i < objs.size(); i++) {
		for (size_t j = 0; j < objs.size(); j++) {
			if (isCollision(objs[j].getCenter(), objs[j].getRadio(), objs[i].getCenter(), objs[i].getRadio()) && i != j) {				
			}
		}
	}
	
	for (size_t i = 0; i < objs.size(); i++){
		objs[i].draw(shader);
		if (i < pigs.size()){
			pigs[i] = objs[i];
		}
		else{
			birds[i - pigs.size()] = objs[i];
		}			
	}			
}

void LevelManager::rebotaContraSuelo(ShaderInfo shader){
	
	

}

void LevelManager::drawBlocks(ShaderInfo shader)
{
	for (auto i = 0; i < blocks.size(); i++) {
		blocks[i].move(blocks[i].getVelocity());
		blocks[i].draw(shader);
	}
}

void  LevelManager::drawSpheres(ShaderInfo shader)
{	
	for (size_t i = 0; i < spheres.size(); i++){
		spheres[i].move(spheres[i].getVelocity());
		spheres[i].draw(shader);
	}
}

unsigned int LevelManager::numLevelsLoaded()
{
	return levels.size();
}