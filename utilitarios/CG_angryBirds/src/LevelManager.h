#ifndef LEVEL_MANAGER_H
#define LEVEL_MANAGER_H

#include <vector>
#include <map>
#include <TinyXML\tinyxml.h>
#include "GlobalVariables.h"
#include "Character.h"
#include "LevelData.h"

typedef unsigned int LEVEL_ID;

class LevelManager
{
private:
	static LevelManager *sInstance;
	std::map<LEVEL_ID, LevelData> levels;
	LEVEL_ID currentLevel;
	std::vector<Character> birds;
	std::vector<Character> pigs;
	std::vector<Character> blocks;
	std::vector<Character> spheres;
	// blocks

	void createCurrentLevel();
public:
	static LevelManager* getInstance();
	~LevelManager();

	void loadLevels(TiXmlHandle *levelsGroup);
	void removeLevels();
	void setCurrentLevel(LEVEL_ID idLevel);
	unsigned int numLevelsLoaded();

	void	drawBackground(ShaderInfo shader);
	void	drawCharacters(ShaderInfo shader);
	void	drawCharacters1(ShaderInfo shader);
	void	drawCharacters2(ShaderInfo shader);
	void	drawCharacters3(ShaderInfo shader);
	void	drawSpheres(ShaderInfo shader);
	void	rebotaContraSuelo(ShaderInfo shader);
	void	drawBlocks(ShaderInfo shader);
	bool	isCollision(glm::vec3 C1,double r1,glm::vec3 C2, double r2);	

	double modulo(glm::vec3 v);
	double crossProduct(glm::vec3, glm::vec3);

};

#endif