# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Team ###
* Marchelo Bragagnini
* Angela Mayhua
* Gerar Quispe

### What is this repository for? ###

Proyecto Angry Birds 3d, en OpenGL 4

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


# Angry-Birds
A cross-platform version of Angry Birds using C++ / OpenGL for PC and ported to C++ / DirectX for the original XBOX
Developed as part of the programming track curriculum at FIEA (Fall 2015).

Watch the demo video at - https://www.youtube.com/watch?v=Sffgre2hctg

## Team

Programmers - Brian Bennett, Nihav Jain
